$(document).ready(function() {
    
    $('.disable-toggle').prop('disabled', true);

    if ($("#day-week").is(':checked')){
        $('#week').prop('disabled', false);
    } else if ($("#day-month").is(':checked')){
        $('#month').prop('disabled', false);
    }

    $('input[type=radio][name=schedule]').on('change', function() {
        if ($(this).attr('id') == 'day-week') {
            $('#week').prop('disabled', false);
            $('#month').prop('disabled', true);
         }else if ($(this).attr('id') == 'day-month' ) {
            $('#month').prop('disabled', false);
            $('#week').prop('disabled', true);
         } else {
             $('.disable-toggle').prop('disabled', true);
         }
    });
});