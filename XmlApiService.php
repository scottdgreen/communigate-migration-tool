<?php

class XmlApiService
{
    private $authUser = 'neo-migrate-tool';
    private $authPass = 'Asdf1234!';
    private $xmlApiUrl = 'http://scott.dev.neonova.net/xml-sat/public_html/NNSAPI.fcgi';
    // private $xmlApiUrl = 'https://xml.NeoNova.net/NNSAPI.fcgi';

    /**
     * Returns an authenticated SimpleXMLElement
     *
     * @access private
     * @param  string $domain   The NovaSub domain in which we are operating
     * @return SimpleXMLElement $document
     */
    private function getAuthenticatedXmlDoc($domain)
    {
        $document = new \SimpleXMLElement('<NeoNovaAPI/>');

        $authenticate = $document->addChild('authenticate');
        $authenticate->addChild('user', $this->authUser);
        $authenticate->addChild('pass', $this->authPass);
        $authenticate->addChild('domain', $domain);

        return $document;
    }
    /**
     * Makes a request to NovaSubscriber XML API
     *
     * @access private
     * @param  SimpleXMLElement $requestDocument (authenticated)
     * @return SimpleXMLElement $resultDocument
     */
    private function makeRequest($requestDocument)
    {
        $xml = $requestDocument->asXml();
        $curl = curl_init($this->xmlApiUrl);

        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));//
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_VERBOSE, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);

        //error_log('XmlApiService makeRequest request = '.$xml."\n",3,'/tmp/scott_log');

        $result = curl_exec($curl);
        //error_log('XmlApiService makeRequest result = '.$result."\n",3,'/tmp/scott_log');
        $resultDocument = new \SimpleXMLElement($result);
        curl_close($curl);

        return $resultDocument;
    }

    /**
     * Makes a get_userinfo request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @return SimpleXMLElement $resultDocument
     */
    public function getUserInfo($domain, $userid)
    {
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('get_userinfo');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a get_children request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @return SimpleXMLElement $resultDocument
     */
    public function getChildren($domain, $userid)
    {
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('get_children');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a check_username_in_use request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @return SimpleXMLElement $resultDocument
     */
    public function checkUserid($domain, $userid)
    {
        // error_log('XmlApiService checkUserid userid = '.$userid."\n",3,'/tmp/scott_log');

        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('check_username_in_use');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a get_uflex_data request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @param  string $fieldname   The Uflex field name
     * @return SimpleXMLElement $resultDocument
     */
    public function getUflexField($domain, $userid, $fieldname)
    {
        //  <get_uflex_data>
        //      <user>
        //          <userid_pattern>testuser</userid_pattern>
        //      </user>
        //      <uflex_field>
        //          <fieldname>cont_phone</fieldname>
        //      </uflex_field>
        //  </get_uflex_data>
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_uflex_data = $requestDocument->addChild('get_uflex_data');
        $user = $get_uflex_data->addChild('user');
        $user->addChild('userid_pattern', $userid);
        $uflex_field = $get_uflex_data->addChild('uflex_field');
        $uflex_field->addChild('fieldname', $fieldname);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a set_uflex_data request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @param  string $fieldname   The Uflex field name
     * @param  string $value       The Uflex value to set
     * @return SimpleXMLElement $resultDocument
     */
    public function setUflexField($domain, $userid, $fieldname, $value)
    {
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_uflex_data = $requestDocument->addChild('set_uflex_data');
        $user = $get_uflex_data->addChild('user');
        $user->addChild('userid', $userid);
        $uflex_field = $get_uflex_data->addChild('uflex_field');
        $uflex_field->addChild('fieldname', $fieldname);
        $uflex_field->addChild('value', $value);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes an add_user request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @param  array $accountData  The user data for the new user
     * @return SimpleXMLElement $resultDocument
     */
    public function createUser($userid, $domain, $accountData)
    {
        // <add_user>
        //    <user>
        //      <userid>bob</userid>
        //      <password>bobobo</password>
        //      <firstname>Bob</firstname>
        //      <lastname>Theuser</lastname>
        //      <childof></childof>
        //      <accountno>12345678</accountno>
        //      <company>The Bob CO.</company>
        //      <address1>1234 Bob Street</address1>
        //      <address2>Suite B O B</address2>
        //      <city>Bobbyville</city>
        //      <state>BS</state>
        //      <zip>00123</zip>
        //      <phone>123 123 1234</phone>
        //	    <phone2>123 123 4567</phone2>
        //	    <callback_number>123 123 1234</callback_number>
        //      <secretq>secret q</secretq>
        //      <secreta>secret a</secreta>
        //    </user>
        //  </add_user>

        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $add_user = $requestDocument->addChild('add_user');
        $user = $add_user->addChild('user');
        foreach ($accountData as $elementName => $element) {
            $sanitizedElement = htmlspecialchars($element, ENT_XML1 | ENT_COMPAT, 'UTF-8');
            if ($elementName == 'userid') { // in case we had to pick a new userid in the destination domain
                $user->addChild('userid', $userid);
            } else {
                $user->addChild($elementName, $sanitizedElement);
            }
        }
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a getnotes request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @return SimpleXMLElement $resultDocument
     */
    public function getNotes($domain, $userid)
    {
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('getnotes');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes getnotes/setnotes request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @param  string $notes       The notes to set
     * @return SimpleXMLElement $resultDocument
     */
    public function appendNotes($domain, $userid, $notes)
    {
        $getNotesReponse = $this->getNotes($domain, $userid);
        $currentNote = '';
        foreach ($getNotesReponse->children() as $element) {
            if ($element->getName() == "notes") {
                $currentNote = $element;
            }
        }

        $notes = $currentNote.$notes;

        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $setnotes = $requestDocument->addChild('setnotes');
        $user = $setnotes->addChild('user');
        $user->addChild('userid', $userid);
        $user->addChild('notes', $notes);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a addforward request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain       The NovaSub domain in which we are operating
     * @param  string $userid       The NovaSub userid
     * @param  string $targetUserid The NovaSub userid where email will be forwarded
     * @param  string $targetDomain The NovaSub domain where email will be forwarded
     * @return SimpleXMLElement $resultDocument
     */
    public function setEmailForwarding($domain, $userid, $targetDomain, $targetUserid)
    {
        //    <addforward>
        //     <user>
        //       <userid>nnstest</userid>
        //       <address>nnstestfwd@mymail.coop</address>
        //     </user>
        //    </addforward>

        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $addforward = $requestDocument->addChild('addforward');
        $user = $addforward->addChild('user');
        $user->addChild('userid', $userid);
        $user->addChild('address', $targetUserid."@".$targetDomain);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a getservices request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @return SimpleXMLElement $resultDocument
     */
    public function getServices($domain, $userid)
    {
        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('getservices');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        return $this->makeRequest($requestDocument);
    }

    /**
     * Makes a change_password request to NovaSubscriber XML API
     *
     * @access public
     * @param  string $domain      The NovaSub domain in which we are operating
     * @param  string $userid      The NovaSub userid
     * @param  string $password    The new password
     * @return SimpleXMLElement $resultDocument
     */
    public function changePassword($domain, $userid, $password)
    {
        // error_log('XmlApiService changePassword userid = '.$userid."\n",3,'/tmp/scott_log');

        $sanitizedPassword = htmlspecialchars($password, ENT_XML1 | ENT_COMPAT, 'UTF-8');

        $requestDocument = $this->getAuthenticatedXmlDoc($domain);
        $get_userinfo = $requestDocument->addChild('change_password');
        $user = $get_userinfo->addChild('user');
        $user->addChild('userid', $userid);
        $user->addChild('password', $sanitizedPassword);
        return $this->makeRequest($requestDocument);
    }

}
