<?php

require_once 'XmlApiService.php';
require_once 'AccountToMigrate.php';
require_once 'vendor/twig/twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => '/path/to/compilation_cache',
));

$domain = $_GET["domain"];
$userid = $_GET["userid"];
$message = $_GET["message"];

$errorEncountered = false;

$account = new AccountToMigrate($userid, $domain);
$accountData = $account->getAccountData();

if ($account->isChild()) {
    $primaryAccount = new AccountToMigrate($accountData['useridofparent'], $domain);
    $accountData = $primaryAccount->getAccountData();
} else {
    $primaryAccount = $account;
}

if ($primaryAccount->getMigrationStatus() == 9 ) {
    $errorEncountered = true;
}

$accountData['isUseridAvailable'] = 'no';
if ($primaryAccount->isUseridAvailable()) {
    $accountData['isUseridAvailable'] = 'yes';
}

$accountData['isArchived'] = 'no';
if ($primaryAccount->isArchived()) {
    $accountData['isArchived'] = 'yes';
}

$accountData['isEmailDisabled'] = 'no';
if ($primaryAccount->isEmailDisabled()) {
    $accountData['isEmailDisabled'] = 'yes';
}

$accountData['isEmailForwarded'] = 'no';
if ($primaryAccount->isEmailForwarded()) {
    $accountData['isEmailForwarded'] = 'yes';
    $accountData['emailForwardAddress'] = $primaryAccount->getEmailForwardAddress();
}

if ($primaryAccount->getTargetUserid !== $userid) {
    $accountData['targetUserid'] = $primaryAccount->getTargetUserid();
}

$accountData['message'] = $primaryAccount->getMessage();

$accountData['migrationStatus'] = $primaryAccount->getMigrationStatus();
$accountData['migrationStatusText'] = $primaryAccount->getMigrationStatusText();
$data = array($accountData);

if ($primaryAccount->isParent()) {
    $children = $primaryAccount->getChildren();
    foreach ($children as $child) {

        $childAccount = new AccountToMigrate($child, $domain);
        $childAccountData = $childAccount->getAccountData();
        if ($childAccount->getMigrationStatus() == 9 ) {
            $errorEncountered = true;
        }
        $childAccountData['isUseridAvailable'] = 'no';
        if ($childAccount->isUseridAvailable()) {
            $childAccountData['isUseridAvailable'] = 'yes';
        }
        $childAccountData['isArchived'] = 'no';
        if ($childAccount->isArchived()) {
            $childAccountData['isArchived'] = 'yes';
        }
        $childAccountData['isEmailDisabled'] = 'no';
        if ($childAccount->isEmailDisabled()) {
            $childAccountData['isEmailDisabled'] = 'yes';
        }
        $childAccountData['isEmailForwarded'] = 'no';
        if ($childAccount->isEmailForwarded()) {
            $childAccountData['isEmailForwarded'] = 'yes';
            $childAccountData['emailForwardAddress'] = $childAccount->getEmailForwardAddress();
        }
        if ($childAccount->getTargetUserid !== $userid) {
            $childAccountData['targetUserid'] = $childAccount->getTargetUserid();
        }
        $childAccountData['message'] = $childAccount->getMessage();
        $childAccountData['migrationStatus'] = $childAccount->getMigrationStatus();
        $childAccountData['migrationStatusText'] = $childAccount->getMigrationStatusText();
        array_push($data, $childAccountData);
    }
}

echo $twig->render('view.twig', array(data => $data, domain => $domain, error => $errorEncountered, message => $message));//