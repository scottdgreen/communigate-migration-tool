<?php
require_once 'XmlApiService.php';
require_once 'AccountToMigrate.php';

$targetDomain = "benlomand.net";

$messageArray = array();
$returnMessage = "Error encountered.  Please contact NeoNova support.";

$userData = $_POST["userData"];
$domain = $_POST["domain"];

$useridOfParent = '';
$targetUserid = '';

$success = true;

foreach ($userData as $userObject) {

    $userid = $userObject['userid'];
    $targetUserid = $userObject['targetUserid'];
    $firstName = $userObject['firstName'];
    $lastName = $userObject['lastName'];
    $password = $userObject['password'];

    // error_log('add_users userid = '.$userid."\n",3,'/tmp/scott_log');

    $account = '';

    $account = new AccountToMigrate($userid, $domain);
    $account->setTargetUserid($targetUserid);

    $accountData = $account->getAccountData();
    if ($account->isParent()) {
        // we assume we will receive the parent userid first
        $useridOfParent = $targetUserid;
    } else {
        $accountData['childof'] = $useridOfParent;
    }

    $accountData['firstname'] = $firstName;
    $accountData['lastname'] = $lastName;
    $accountData['password'] = $password;

    $account->setAccountData($accountData);

    $account->createTargetUser();
    array_push($messageArray,  $account->getMessage()." ");

    error_log('add_users '.$userid.' getMessage = '.$account->getMessage()."\n",3,'/tmp/scott_log');

    error_log('add_users '.$userid.' migrationStatus = '.$account->getMigrationStatus()."\n",3,'/tmp/scott_log');

    if ($account->getMigrationStatus() == '1') {
        $account->changePassword();
        //array_push($messageArray, $account->getMessage()." ");

        $account->writeToQueue();
        //array_push($messageArray, $account->getMessage()." ");

        $account->setEmailForwarding();
        //array_push($messageArray, $account->getMessage()." ");
    }

    if ($account->getMigrationStatus() == '9') {
        $success = false;
    }

}

if ($success) {
    $returnMessage = "Migration successfully initiated.  Please allow 24 hours to complete.";
}

//foreach($messageArray as $message) {
//    $returnMessage = $returnMessage . $message;
//}

error_log('add_users returnMessage = '.$returnMessage."\n",3,'/tmp/scott_log');

header("Content-Type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n";
echo "<status>\n";
echo "<message>".$returnMessage."</message>\n";
echo "</status>\n";