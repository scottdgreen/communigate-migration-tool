<?php
require_once 'XmlApiService.php';

$targetDomain = "benlomand.net";

$message = "User ID not available";

$userid = $_GET["userid"];

$xmlService = new XmlApiService();

// <status>
// <message>
// Username not available.
// </message>
// </status>

$checkUseridReponse = $xmlService->checkUserid($targetDomain, $userid);
foreach ($checkUseridReponse->children() as $element) {
    if ($element->getName() == "message" && (strpos(strtolower($element), 'username available') !== false)) {
        $message = "User ID available";
    }
}

header("Content-Type: text/xml");
echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' . "\n";
echo "<status>\n";
echo "<message>".$message."</message>\n";
echo "</status>\n";

