<?php
require_once 'XmlApiService.php';

class AccountToMigrate
{
    private $targetDomain = "benlomand.net";
    private $deactivationPassword = "deActivat3d!";

    private $pdoSatDsn = 'mysql:host=db.dev.neonova.net;dbname=sat_master_scott';
    private $pdoSatUser = 'root';
    private $pdoSatPassword = 'watw84';

    private $pdoPhxDsn = 'mysql:host=db.dev.neonova.net;dbname=v1_nns_master_scott';
    private $pdoPhxUser = 'root';
    private $pdoPhxPassword = 'watw84';

    private $pdoHarvestDsn = 'mysql:host=gmms10.neonova.net;dbname=harvest_benlomand';
    private $pdoHarvestUser = 'harvest';
    private $pdoHarvestPassword = 'h4rV3StFUnt1m3!';

    private $pathToLog = '/tmp/scott_log';

    private $userid;
    private $domain;

    private $accountData = array();
    private $targetUserid;
    private $targetUid;
    private $uid;
    private $phxUid;
    private $emailForwardAddress;
    private $emailForwarded = false;
    private $emailDisabled = false;

    private $xmlService;

    private $useridAvailable = false;
    private $migrationStatus = 0;
    private $migrationStatusText = array(
        0 => "Not started",
        1 => "In migration",
        2 => "Migration complete",
        9 => "Error"
    );

    private $alternateElementNames = array(
        'question' => 'secretq',
        'answer'   => 'secreta',
    );

    private $domainId = array(
        'blomand.net'   => 391,
        'volfirst.net'  => 384,
        'benlomand.net' => 728
    );

    private $trendMicroLicenses = array();
    private $hasPhxTrendMicroLicenses = false;

    private $parent = false;
    private $child = false;
    private $archived = false;
    private $message;

    private $children = array();

    private $hasTrendMicroLicenses = false;

    public function isUseridAvailable()
    {
        return $this->useridAvailable;
    }

    public function isParent()
    {
        return $this->parent;
    }

    public function isChild()
    {
        return $this->child;
    }

    public function isArchived()
    {
        return $this->archived;
    }

    public function isEmailDisabled()
    {
        return $this->emailDisabled;
    }

    public function isEmailForwarded()
    {
        return $this->emailForwarded;
    }

    public function getEmailForwardAddress()
    {
        return $this->emailForwardAddress;
    }

    public function getAccountData()
    {
        return $this->accountData;
    }

    public function setAccountData($accountData)
    {
        $this->accountData = $accountData;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getMigrationStatus()
    {
        return $this->migrationStatus;
    }

    public function getMigrationStatusText()
    {
        $status = (int) $this->migrationStatus;
        return $this->migrationStatusText[$status];
    }

    public function setMigrationStatus($status)
    {
        $this->migrationStatus = $status;
    }

    public function getTargetUserid()
    {
        return $this->targetUserid;
    }

    public function setTargetUserid($targetUserid)
    {
        $this->targetUserid = $targetUserid;
    }

    function __construct($userid, $domain)
    {
        $this->userid = $userid;
        $this->targetUserid = $userid;
        $this->domain = $domain;

        $this->xmlService = new XmlApiService();

        $this->checkTrendMicroLicenses();
        $this->checkEmailForwarding();

        $getUserInfoResponse = $this->xmlService->getUserInfo($domain, $userid);
        foreach ($getUserInfoResponse->children() as $elementName => $element) {
            if (array_key_exists($elementName, $this->alternateElementNames)) {
                $alternateElementName = $this->alternateElementNames[$elementName];
                $this->accountData[$alternateElementName] = (string) $element;
            } else {
                $this->accountData[$elementName] = (string) $element;
            }
        }

        if (strtolower($this->accountData['isarchived']) == 'yes') {
            $this->archived = true;
        }

        $checkUseridReponse = $this->xmlService->checkUserid($this->targetDomain, $userid);
        $message = (string) $checkUseridReponse->message;
        if (trim($message) == "Username available.") {
            $this->useridAvailable = true;
        }

//        <response>
//          <uflex_data uid='610220'>
//            <userid>neotest</userid>
//            <fieldname>communigate_migration_status</fieldname>
//            <value>0</value>
//          </uflex_data>
//          <code>
//            1
//          </code>
//        </response>

        $getMigrationStatusResponse = $this->xmlService->getUflexField($domain, $userid, 'communigate_migration_status');
        $value = (string) $getMigrationStatusResponse->uflex_data->value;
        $this->migrationStatus = $value;
        error_log('AccountToMigrate construct migrationStatus = '.$value."\n",3,$this->pathToLog);

        $this->checkQueue();

        $getMigrationTargetResponse = $this->xmlService->getUflexField($domain, $userid, 'communigate_migration_target');
        $value = (string) $getMigrationTargetResponse->uflex_data->value;
        if (strlen(trim($value)) > 0) {
            $this->targetUserid = $value;
        }
        error_log('AccountToMigrate construct targetUserid = '.$value."\n",3,$this->pathToLog);

        $getMigrationMessageResponse = $this->xmlService->getUflexField($domain, $userid, 'communigate_migration_message');
        $value = (string) $getMigrationMessageResponse->uflex_data->value;
        $this->message = $value;
        error_log('AccountToMigrate construct message = '.$value."\n",3,$this->pathToLog);


        if ($this->accountData['useridofparent']) {
            $this->child = true;
        }

        if (strtolower($this->accountData['haschildren']) == 'yes') {

            $this->parent = true;

            //<response>
            //    <result>
            //        <child_count>2</child_count>
            //        <child>first_child_of_bob</child>
            //        <child>second_child_of_bob</child>
            //    </result>
            //    <status>
            //        <code>
            //                1
            //        </code>
            //    </status>
            //</response>

            $response = $this->xmlService->getChildren($domain, $userid);
            $result = $response->result;
            foreach ($result->children() as $element) {
                if ($element->getName() == 'child') {
                    array_push($this->children, $element);
                }
            }
        }

    }

    public function createTargetUser()
    {
        // <add_user>
        //    <user>
        //      <userid>bob</userid>
        //      <password>bobobo</password>
        //      <firstname>Bob</firstname>
        //      <lastname>Theuser</lastname>
        //      <childof></childof>
        //      <accountno>12345678</accountno>
        //      <company>The Bob CO.</company>
        //      <address1>1234 Bob Street</address1>
        //      <address2>Suite B O B</address2>
        //      <city>Bobbyville</city>
        //      <state>BS</state>
        //      <zip>00123</zip>
        //      <phone>123 123 1234</phone>
        //	    <phone2>123 123 4567</phone2>
        //	    <callback_number>123 123 1234</callback_number>
        //      <secretq>secret q</secretq>
        //      <secreta>secret a</secreta>
        //    </user>
        //  </add_user>

        $createTargetUserResponse =
            $this->xmlService->createUser($this->targetUserid, $this->targetDomain, $this->accountData);

        // <status>
        //  <message>
        //    Added user.
        // </message>
        //  <code>
        //    1
        //  </code>
        // </status>

        $message = (string) $createTargetUserResponse->message;
        $this->message = $message;
        $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_message', $this->message);

        $this->setTargetUid();
        // $this->checkTrendMicroLicenses();

        // if the userid isn't available on benlomand.net, set the 'communigate_migration_target' uflex field to the
        // alternate id selected by the user

        $this->migrationStatus = '1';
        if (strpos(strtolower($this->message), 'added user') !== false) {

            $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_target', $this->targetUserid);
            $this->xmlService->appendNotes($this->targetDomain, $this->targetUserid, "Created by communigate-migration-tool. Source: ".$this->userid."@".$this->domain.". ");

            if ($this->hasTrendMicroLicenses) {
                $this->deletePhxTrendMicroLicenses();
                if ($this->hasPhxTrendMicroLicenses == false) {
                    $this->migrateTrendMicroLicenses();
                } else {
                    $this->migrationStatus = '9';
                    $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_message', "Datasync error.");
                }
            }

        } else {
            $this->migrationStatus = '9';
        }
        $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_status', $this->migrationStatus);

    }

    public function checkTrendMicroLicenses()
    {

//        private $pdoSatDsn = 'mysql:host=db.dev.neonova.net;dbname=sat_master_scott'
//        private $pdoSatUser = 'root';
//        private $pdoSatPassword = 'watw84';

//        $pdoSat = new PDO('mysql:host=db.dev.neonova.net;dbname=sat_master_scott',
//            'root',
//            'watw84',
//            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $pdoSat = new PDO($this->pdoSatDsn,
            $this->pdoSatUser,
            $this->pdoSatPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        // grab the uid because we will need it later

        $stmt = $pdoSat->prepare('SELECT license, uid ' .
            'FROM spirit_tm_keys AS stk ' .
            'JOIN domain AS d ON (stk.did = d.did) ' .
            'WHERE d.domain = :domain ' .
            'AND stk.deleted = 0 '.
            'AND stk.userid = :userid');
        $stmt->execute(array('domain' => $this->domain, 'userid' => $this->userid));
        //$licenses = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        $rows = $stmt->fetchAll();
        $stmt->closeCursor();

        //error_log('AccountToMigrate checkTrendMicro # rows = '.count($rows)."\n",3,$this->pathToLog);

        if (count($rows) > 0) {
            $this->hasTrendMicroLicenses = true;
            $this->uid = $rows[0][1];
            //error_log('AccountToMigrate checkTrendMicro uid = '.$rows[0][1]."\n",3,$this->pathToLog);
            foreach($rows as $row) {
                array_push($this->trendMicroLicenses, $row[0]);
                error_log('AccountToMigrate checkTrendMicro pushing license '.$row[0]." \n",3,$this->pathToLog);
            }
        }

    }

    public function migrateTrendMicroLicenses()
    {
//        $pdoSat = new PDO('mysql:host=db.dev.neonova.net;dbname=sat_master_scott',
//            'root',
//            'watw84',
//            //array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
//            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));

        $pdoSat = new PDO($this->pdoSatDsn,
            $this->pdoSatUser,
            $this->pdoSatPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        // update spirit_tm_keys set uid = 2932671, did = 728 where uid = 625411 and userid = 'mdd2' and did = 391;

        $stmt = $pdoSat->prepare('UPDATE spirit_tm_keys ' .
                                                'SET did = :targetDid, ' .
                                                'uid = :targetUid ' .
                                                'WHERE did = :did ' .
                                                'AND deleted = 0 '.
                                                'AND userid = :userid');
        $return = $stmt->execute(array(
                        'targetDid' => $this->domainId[$this->targetDomain],
                        'targetUid' => $this->targetUid,
                        'did'       => $this->domainId[$this->domain],
                        'userid'    => $this->userid));

        if ($return) {
            $this->message = "Migrated TrendMicro license(s). ";
        } else {
            $this->migrationStatus = '9';
            $this->message = "Error migrating TrendMicro license(s). ";
        }
        $stmt->closeCursor();
        $this->appendToUflexMigrationMessage($this->message);

        //error_log('AccountToMigrate checkTrendMicro # rows affected = '.$affected."\n",3,$this->pathToLog);

        foreach ($this->trendMicroLicenses as $license) {
            $this->xmlService->appendNotes($this->targetDomain, $this->targetUserid,
                "TrendMicro license " . $license . " migrated from " . $this->domain . " by communigate-migration-tool. ");
        }

    }

    public function setTargetUid()
    {
//        $pdoSat = new PDO('mysql:host=db.dev.neonova.net;dbname=sat_master_scott',
//            'root',
//            'watw84',
//            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $pdoSat = new PDO($this->pdoSatDsn,
            $this->pdoSatUser,
            $this->pdoSatPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $pdoSat->prepare('SELECT uid ' .
            'FROM user AS u ' .
            'JOIN domain AS d ON (u.did = d.did) ' .
            'WHERE d.domain = :domain ' .
            'AND u.userid = :userid');
        $stmt->execute(array('domain' => $this->targetDomain, 'userid' => $this->targetUserid));

        // TODO use fetch instead of fetchAll?

        $return = $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
        $stmt->closeCursor();

        if (count($return) == 1) {
            $this->targetUid = $return[0];
            //error_log('AccountToMigrate setTargetUid = ' . $this->targetUid . "\n", 3, $this->pathToLog);
        }

    }

    public function setPhxUid()
    {

        //    private $pdoPhxDsn = 'mysql:host=db.dev.neonova.net;dbname=v1_nns_master_scott';
        //    private $pdoPhxUser = 'root';
        //    private $pdoPhxPassword = 'watw84';

        $phxSat = new PDO($this->pdoPhxDsn,
            $this->pdoPhxUser,
            $this->pdoPhxPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $phxSat->prepare('SELECT uid ' .
            'FROM user ' .
            'WHERE sat_uid = :uid');
        $stmt->execute(array('uid' => $this->uid));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        $this->phxUid = $result['uid'];

    }

    public function deletePhxTrendMicroLicenses()
    {
        $this->setPhxUid();

//        $phxSat = new PDO('mysql:host=db.dev.neonova.net;dbname=v1_nns_master_scott',
//            'root',
//            'watw84',
//            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $phxSat = new PDO($this->pdoPhxDsn,
            $this->pdoPhxUser,
            $this->pdoPhxPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $phxSat->prepare('DELETE ' .
            'FROM feature_trendmicro ' .
            'WHERE uid = :uid');
        $response = $stmt->execute(array('uid' => $this->phxUid));
        $stmt->closeCursor();

    }

    public function checkPhxTrendMicroLicenses()
    {

        $phxSat = new PDO($this->pdoPhxDsn,
            $this->pdoPhxUser,
            $this->pdoPhxPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

//        $phxSat = new PDO('mysql:host=db.dev.neonova.net;dbname=v1_nns_master_scott',
//            'root',
//            'watw84',
//            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $phxSat->prepare('SELECT license ' .
            'FROM feature_trendmicro ' .
            'WHERE uid = :uid');
        if ($stmt->execute(array('uid' => $this->phxUid))) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if (count($result) > 0) {
                $this->hasPhxTrendMicroLicenses = true;
            }
        } else {
            $this->migrationStatus = '9';
            $this->message = "Error checking Phoenix TrendMicro licenses. ";
            $this->appendToUFlexMigrationMessage($this->message);
        }

        $stmt->closeCursor();

    }

    public function changePassword()
    {
        $response = $this->xmlService->changePassword($this->domain, $this->userid, $this->deactivationPassword);
        $message = (string) $response->message;
        $this->message = $message;
        if (trim($message) !== "Changed password.") {
            $this->migrationStatus = '9';
            $this->message = "Error changing password. ";
        }
        $this->appendToUFlexMigrationMessage($this->message);
    }

    public function checkEmailForwarding()
    {
        $response = $this->xmlService->getServices($this->domain, $this->userid);
        if (isset($response->services->email)) { // if this is not set, email is disabled
            $forwardAddress = (string)$response->services->email->forward->address;
            error_log('AccountToMigrate checkEmailForwarding '. $this->userid . ' strlen = ' . strlen(trim($forwardAddress)) . "\n", 3, $this->pathToLog);
            if (strlen(trim($forwardAddress)) > 0) {
                $this->emailForwarded = true;
                $this->emailForwardAddress = trim($forwardAddress);
                error_log('AccountToMigrate checkEmailForwarding emailForwarded = ' . $this->emailForwarded . "\n", 3, $this->pathToLog);
            }
        } else {
            $this->emailDisabled = true;
            error_log('AccountToMigrate checkEmailForwarding '. $this->userid . ' emailDisabled' . "\n", 3, $this->pathToLog);
        }

    }

    public function setEmailForwarding()
    {
        $response = $this->xmlService->setEmailForwarding($this->domain, $this->userid, $this->targetDomain, $this->targetUserid);
        $message = (string) $response->message;
        error_log('AccountToMigrate setEmailForwarding message = ' . $message . "\n", 3, $this->pathToLog);
        $this->message = $message;
        if (trim($message) !== "Added forward.") {
            $this->migrationStatus = '9';
            $this->message = "Error adding forward. ";
        }
        $this->appendToUFlexMigrationMessage($this->message);
    }

    public function appendToUflexMigrationMessage($message)
    {
//        <response>
//          <uflex_data uid='610220'>
//            <userid>neotest</userid>
//            <fieldname>communigate_migration_status</fieldname>
//            <value>0</value>
//          </uflex_data>
//          <code>
//            1
//          </code>
//        </response>

        $getMigrationMessageResponse = $this->xmlService->getUflexField($this->domain, $this->userid, 'communigate_migration_message');
        $currentMessage = $getMigrationMessageResponse->uflex_data->value;
        $message = $currentMessage . " " . $message;
        $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_message', $message);
    }

    public function writeToQueue()
    {
        //    MySQL Server: gmms10.neonova.net
        //    User: harvest
        //    Pass: h4rV3StFUnt1m3!

        //    SRCSVR = 137.118.45.111
        //    DSTSVR = 137.118.44.5
        //    SRCU = Source Server Email Address for the User
        //    SRCP = PASS (or anything)
        //    DSTU = Destination Server Email Address for the User. (Likely the same as SRCU just with @benlomand.net instead of @blomand.net)
        //    DSTP = PASS (or anything)
        //    PRIO = 50000
        //    STATUS = Q
        //    PASS = 1

        //        private $pdoHarvestDsn = 'mysql:host=gmms10.neonova.net;dbname=harvest_benlomand';
        //        private $pdoHarvestUser = 'harvest';
        //        private $pdoHarvestPassword = 'h4rV3StFUnt1m3!';

        $harvestSat = new PDO($this->pdoHarvestDsn,
            $this->pdoHarvestUser,
            $this->pdoHarvestPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $harvestSat->prepare('INSERT INTO QUEUE ' .
            'SET SRCSVR = :srcsvr, ' .
            'DSTSVR = :dstsvr, ' .
            'SRCU = :srcu, ' .
            'SRCP = :srcp, ' .
            'DSTU = :dstu, ' .
            'DSTP = :dstp, ' .
            'PRIO = :prio, ' .
            'STATUS = :status, ' .
            'PASS = :pass ');
        $return = $stmt->execute(array(
            'srcsvr' => "137.118.45.111",
            'dstsvr' => "137.118.44.5",
            'srcu'   => $this->userid."@".$this->domain,
            'srcp'   => "",
            'dstu'   => $this->targetUserid."@".$this->targetDomain,
            'dstp'   => "",
            'prio'   => "5000",
            'status' => "Q",
            'pass'   => "1"));
        if ($return) {
            $this->message = "Userid inserted into migration queue. ";
        } else {
            $this->migrationStatus = '9';
            $this->message = "Error inserting userid into migration queue. ";
        }
        $stmt->closeCursor();
        $this->appendToUflexMigrationMessage($this->message);
        // error_log('AccountToMigrate writeToQueue return = ' . $return . "\n", 3, $this->pathToLog);

    }

    public function checkQueue()
    {
        $harvestSat = new PDO($this->pdoHarvestDsn,
            $this->pdoHarvestUser,
            $this->pdoHarvestPassword,
            array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $stmt = $harvestSat->prepare('SELECT STATUS ' .
            'FROM QUEUE ' .
            'WHERE SRCU = :email');
        $stmt->execute(array('email' => $this->userid."@".$this->domain));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();

        // error_log('AccountToMigrate checkQueue result = '.$result['STATUS']."\n",3,$this->pathToLog);

        $status = $result['STATUS'];
        if ($status == "Q" || $status == "R" || $status == "D") {
            $this->migrationStatus = 1;
        } elseif ($status == "F") {
            $this->migrationStatus = 2;
        } else {
            $this->migrationStatus = 0;
        }
        $this->xmlService->setUflexField($this->domain, $this->userid, 'communigate_migration_status', $this->migrationStatus);
    }

}